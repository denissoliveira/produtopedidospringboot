package br.com.estudo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProdutoPedidoSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProdutoPedidoSpringBootApplication.class, args);
	}

}
